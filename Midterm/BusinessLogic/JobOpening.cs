﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Midterm.BusinessLogic
{
    List<EducationLevel> edu = new List<EducationLevel>();
    class JobOpening
    {
        readonly int id; //change to readonly property
        public string companyName;  //change to public read - private write property
        readonly string title;  //change to read-write property
        readonly int experience; //change to read-write property

        //JobType as enum
        enum JobType { good, bad }
        //Instance of PayBand
        int minimumPay;
        int maximumPay;
        int yearlyStep;
        public JobOpening(int minPay, int maxPay, int yearly)
        {
            minimumPay = minPay;
            maximumPay = maxPay;
            yearlyStep = yearly;
            
            if (minPay <=0)
            {
                minPay = 1;
            }
            if (maxPay <=0)
            {
                maxPay = 1;
            }
            if (maxPay <= minPay)
            {
                maxPay = maxPay + 1;
            }
            if (yearly <=0)
            {
                yearly = 1;
            }
        }

        //Constructor
        public JobOpening(string name, string jobTitle, int jobExperience)
        {
            companyName = name;
            title = jobTitle;
            experience = jobExperience;

            if (name == null)
            {
                //Console.WriteLine("Please enter a Company Name")
                name = "Sample";
            }

            if (jobTitle == null)
            {
                //Console.WriteLine("Please enter a job title")
                jobTitle = "Sample";
            }

            if (jobExperience <=0)
            {
                //Console.WriteLine("Please enter a value greater than 0")
                jobExperience = 1;
            }
        }
    }
}
